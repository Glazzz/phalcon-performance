<?php

$router = $di->getRouter();

$router->addGet('/', [
    'controller' => 'index',
    'action' => 'index',
]);

$router->addGet('/exchange/add', [
    'controller' => 'Exchanges',
    'action' => 'add',
]);

$router->addPost('/exchange/save', [
    'controller' => 'Exchanges',
    'action' => 'save',
]);


$router->handle();
