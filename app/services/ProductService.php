<?php

use Phalcon\Mvc\Model\Query;

class ProductService
{
    protected $db;

    protected $di;

    protected $manager;

    public function __construct()
    {
        global $di;
        $this->di = $di;
        $this->db = $di->get('db');
        $this->manager = $di->get('modelsManager');
    }

    public function deleteAllProducts()
    {
        $this->manager->executeQuery('Delete FROM Product');
    }

    private function showInfo($finish, $counter = null)
    {
        printf('<p>Time spent: %s</p>', $finish);
        printf('<p>Memory used: %s megabytes</p>', round(memory_get_usage(true) / 1048576,2));

        if ($counter) {
            printf('<p>%d rows.</p>', $counter);
            printf('<p>Requests per seconds: %s</p>', $counter / $finish);
        }

        die;
    }

    public function insertPerformance()
    {
        $this->deleteAllProducts();

        $categories = $this->manager->createQuery('SELECT * FROM Category')->execute();

        $counter = 0;

        $start = microtime(true);

        foreach (range(0, 100) as $key => $item) {
            /** @var Category $category */
            foreach ($categories as $catKey => $category) {
                $product = new Product();

                $product->setName(sprintf('product-%s_%s', $key, $catKey))
                    ->setCategoryId($category->getId())
                    ->setDescription(
                        sprintf(
                            'Product Description + category description %s',
                            $category->getDescription()
                        )
                    );

                $product->save();

                $counter++;
            }
        }

        $finish = microtime(true) - $start;

        $this->showInfo($finish, $counter);
    }

    public function findByNamePerformance($productName = 'product-2_4')
    {
        $start = microtime(true);

        foreach (range(0, 1000) as $item) {
            $this->manager->executeQuery('SELECT * FROM Product WHERE Product.name = :name:', [
                'name' => $productName,
            ]);
        }

        $finish = microtime(true) - $start;

        $this->showInfo($finish, 1000);
    }

    public function findByCategoryIdPerformance($categoryId = 1)
    {
        $start = microtime(true);

        foreach (range(0, 1000) as $item) {
            $this->manager->executeQuery('SELECT * FROM Product WHERE Product.category_id = :categoryId:', [
                'categoryId' => $categoryId,
            ]);
        }

        $finish = microtime(true) - $start;

        $this->showInfo($finish, 1000);
    }

    public function findByLikeTextPerformance($text = 'Product Description')
    {
        $start = microtime(true);

        foreach (range(0, 1000) as $item) {
            $this->manager->executeQuery('SELECT * FROM Product WHERE Product.description LIKE "%:text:%"', [
                'description' => $text,
            ]);
        }

        $finish = microtime(true) - $start;

        $this->showInfo($finish, 1000);
    }

    public function findByName($productName = 'product-2_4')
    {
        return $this->manager->executeQuery('SELECT * FROM Product WHERE Product.name = :name:', [
            'name' => $productName,
        ]);
    }

    public function insertProducts()
    {
        $categories = $this->manager->createQuery('SELECT * FROM Category')->execute();

        $products = [];

        /** @var Category $category */
        foreach ($categories as $catKey => $category) {
            $product = new Product();

            $product->setName(sprintf('product-%s_%s', 'A', $catKey))
                ->setCategoryId($category->getId())
                ->setDescription(
                    sprintf(
                        'Product Description + category description %s',
                        $category->getDescription()
                    )
                );

            $product->save();

            $products[] = $product;
        }

        return $products;
    }

    public function findByLikeText($text = 'Product Description')
    {
        return $this->manager->executeQuery('SELECT * FROM Product WHERE Product.description LIKE "%:text:%"', [
            'description' => $text,
        ]);
    }
}