<?php

use Phalcon\Http\Request;
use Phalcon\Http\Response;

class PerformanceController extends ControllerBase
{
    /**
     * @var ProductService
     */
    protected $productService;

    protected $request;

    protected $response;

    public function onConstruct()
    {
        $this->productService = new ProductService();
        $this->request = new Request();
        $this->response = new Response();
    }

    public function indexAction()
    {

    }

    public function insertProductsAction()
    {
        $this->productService->insertPerformance();
    }

    public function findByNameAction()
    {
        $this->productService->findByNamePerformance($this->request->getQuery('name', null, 'product-2_4'));
    }

    public function findProductsByCategoryIdAction()
    {
        $this->productService->findByCategoryIdPerformance($this->request->getQuery('category_id', null, 1));
    }

    public function findProductsByTextAction()
    {
        $this->productService->findByLikeTextPerformance($this->request->getQuery('text', null, 'Product Description'));
    }

    public function singleFindByNameAction()
    {
        $this->view->disable();

        $this->response->setJsonContent(json_encode($this->productService->findByName()));

        return $this->response;
    }

    public function singleInsertProductsAction()
    {
        $this->view->disable();

        $this->response->setContent(json_encode($this->productService->insertProducts()));

        return $this->response;
    }

    public function singleFindByLikeTextAction()
    {
        $this->view->disable();

        $this->response->setJsonContent($this->productService->findByLikeText());

        return $this->response->send();
    }
}
