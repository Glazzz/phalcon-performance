<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class CategoriesMigration_103
 */
class CategoriesMigration_103 extends Migration
{
    protected $connection;

    public function __construct()
    {
        $this->connection = self::$_connection;
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {
        $categories = [
            [
                'id' => 1,
                'name' => 'category 1',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sed euismod velit. Vivamus et est a erat blandit auctor rhoncus sed turpis. Integer a iaculis ante, porttitor placerat leo. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur vulputate ex enim, at ultricies ante commodo et. Etiam pellentesque ligula at accumsan pretium. Ut risus dolor, dictum nec viverra a, luctus sed tellus. Integer ac hendrerit tortor. Sed blandit consectetur tortor, eget laoreet neque facilisis a. Nam at interdum mauris, vitae vehicula sapien. Nullam pellentesque arcu eu turpis auctor, non mollis dui eleifend. Fusce eget magna sed leo feugiat ullamcorper. Aenean rhoncus mi et arcu commodo, ut dictum ligula ornare. Maecenas ante odio, laoreet et dolor et, mattis ornare erat. Vivamus diam libero, cursus ut luctus ut, sodales luctus erat. Curabitur eget velit nec orci dapibus posuere ac eu eros.',
            ],
            [
                'id' => 2,
                'name' => 'category 1',
                'description' => 'Integer aliquet eu velit in malesuada. Praesent malesuada eu lorem et hendrerit. Suspendisse maximus neque ac finibus pharetra. Sed placerat mi et lacus mattis pellentesque. Duis et egestas lectus. Donec eget malesuada neque, non ullamcorper lacus. Ut pharetra vehicula dapibus. Nullam vel tempor metus, et eleifend magna. Duis semper dapibus varius. Donec tempor leo eget eros pretium convallis et eleifend magna. Fusce at sagittis felis, id placerat purus. Nulla facilisi. Suspendisse justo eros, tincidunt in purus ut, varius semper dui. Nunc libero sapien, ornare id nunc ac, aliquam imperdiet nibh. Aliquam tempus semper erat, id ullamcorper odio mattis sed. Sed hendrerit nisi quis nisl venenatis, vel cursus tortor viverra.',
            ],
            [
                'id' => 3,
                'name' => 'category 1',
                'description' => 'In iaculis augue id arcu blandit, vitae accumsan lacus volutpat. Vivamus purus urna, cursus in nunc sit amet, pretium scelerisque nibh. Fusce lobortis placerat nibh, ut hendrerit purus pretium in. Suspendisse eu quam ut sapien rhoncus semper. Etiam scelerisque leo sed ultricies accumsan. Vestibulum sagittis pellentesque vehicula. Sed non interdum nibh, nec consectetur lorem. Phasellus porta interdum nisi, ut consequat risus euismod id. Mauris at pretium nisl. Pellentesque urna leo, faucibus eu risus nec, pellentesque dapibus nunc.',
            ],
            [
                'id' => 4,
                'name' => 'category 1',
                'description' => 'Ut pretium commodo risus quis placerat. Phasellus elit ipsum, volutpat at enim ac, convallis gravida quam. Aenean in tellus in quam lacinia accumsan non in lectus. Donec at cursus nisi. Ut ultricies, risus finibus scelerisque pharetra, dui velit dictum ligula, non lacinia leo velit in dolor. Sed nec libero ipsum. Sed pretium suscipit felis, sed elementum velit aliquam eu. Praesent et pretium libero, at elementum leo. Fusce ultrices nisi in turpis mattis, eu volutpat elit condimentum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam semper nibh nec bibendum viverra. Proin fermentum felis ac nisl hendrerit tempus. Vestibulum nisi sem, pellentesque eget arcu tincidunt, posuere suscipit nulla. Donec tempus nisl quis lorem fringilla suscipit.',
            ],
            [
                'id' => 5,
                'name' => 'category 1',
                'description' => 'Sed nulla nisl, auctor sit amet venenatis euismod, finibus ut nulla. Morbi finibus ultricies massa placerat consequat. Phasellus leo est, ornare eget tincidunt a, sollicitudin vel neque. Integer commodo sollicitudin libero. Quisque in aliquet lorem. Nulla a risus tortor. Nam nunc metus, viverra id tempor et, sodales ac felis.',
            ],
            [
                'id' => 6,
                'name' => 'category 1',
                'description' => '',
            ],
        ];

        $this->clearCategories();

        foreach ($categories as $item) {
            $this->connection->insert(
                'categories',
                [
                    $item['name'],
                    $item['description'],
                ],
                [
                    'name',
                    'description'
                ]
            );
        }
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {
        $this->clearCategories();
    }

    public function clearCategories()
    {
        $this->connection->delete('categories');
    }
}
